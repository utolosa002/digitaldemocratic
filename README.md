[English](https://gitlab.com/digitaldemocratic/digitaldemocratic/-/blob/master/README_en.md) - [Català](https://gitlab.com/digitaldemocratic/digitaldemocratic/-/blob/master/README.md)

### Llicència

AGPLv3 (https://www.gnu.org/licenses/agpl-3.0.en.html)

### Crèdits

Projecte pilot del Pla de Digitalització Democràtica dirigit per Xnet i famílies promotores. Programari creat per IsardVDI i 3iPunt amb la col·laboració de MaadiX.net, Affac, l’Ajuntament de Barcelona i el Consorci d'Educació de Barcelona.

# Què és això

Aquest projecte permet facilitar un proveïdor d'identitat complet i moltes aplicacions per tenir un entorn pensat per a escoles i  universitats. El projecte proporcionarà una solució integrada per a  gestionar l'entorn comú en l'educació:

- **Aules**: Una instància de Moodle amb tema personalitzat i connectors personalitzats.
- **Fitxers**: Una instància del Nextcloud amb tema personalitzat i connectors personalitzats.
- **Documents**: Una instància d'OnlyOffice integrada amb Nextcloud.
- **Pàgines web**: Una instància de pressió de paraules amb el tema personalitzat i connectors personalitzats.
- **Pad**: Una instància etherpad integrada amb Nextcloud.
- **Conferències**: Un BigBlueButton integrat amb Moodle i Nextcloud. Necessita un servidor independent.
- **Formularis**: Els connectors del Nextcloud dels formularis.
- ... (algunes aplicacions com jitsi o BigBlueButton no estan totalment integrades ara mateix)

|                              |                                 |
| ---------------------------- | ------------------------------- |
| ![](docs/img/classrooms.png) | ![](docs/img/cloud_storage.png) |

# Interfície d'administració

Ara hi ha una interfície d'administració que permet gestionar fàcilment usuaris i grups i mantenir-los sincronitzats entre aplicacions. Això es fa executant accions sobre les apis de les diferents aplicacions.

| ![](docs/img/admin_sync.png) | ![](docs/img/admin_user_edit.png) |
| ---------------------------- | --------------------------------- |

Per migrar i introduïr fàcilment usuaris i grups al sistema també hi ha dues importacions::

- Des de la consola d'administració de Google en format JSON
- Des d'un fitxer CSV

Aquesta interfície d'administració està en estat alfa, però ja permet gestionar usuaris sincronitzats entre Keycloak, Moodle i Nextcloud.

# Estat del projecte

Funcional, però hi seguim treballant i tindrà moltes millores en els pròxims mesos. Cal fer algunes automatitzacions, especialment amb la integració de SAML a Moodle i Keycloak.

La vostra col·laboració és benvinguda! Podeu fer un *fork* del projecte per a desenvolupar o bé obrir-nos *incidències* en aquest repositori.

# Documentació DigitalDemocratic

Està escrita en markdown utilitzant [MkDocs+Gitlab](https://gitlab.com/pages/mkdocs).

Mire al directori `docs` els fitxers Markdown o bé a [auto-built site](https://digitaldemocratic.gitlab.io/digitaldemocratic).

## Inici ràpid

```
cp digitaldemocratic.conf.sample digitaldemocratic.conf
```

Canvia les contrasenyes per defecte

```
./securize_conf.sh
```

Editeu les variables del fitxer digitaldemocratic.conf per satisfer les vostres necessitats.

```
cp -R custom.sample custom
```

Edita i substitueix els fitxers per personalitzar el sistema.

La primera vegada executa:
```
./dd-ctl repo-update
```

I després:
```
./dd-ctl all
```

NOTA: L'autenticació SAML actualment es troba automatitzada:

- Moodle: No completament automatitzat.
  1. Inicieu la sessió a Moodle com a administrador via: https://moodle.\<domain\>/login/index.php?saml=off
  2. Aneu a la configuració d'autenticació: https://moodle.\<domain\>/admin/settings.php?section=manageauths
  3. Activa SAML2 fent clic a l'ull.
  4. Clic a *configuració* a SAML2
  5. Feu clic al botó *Regenera el certificat* dins del formulari. Després d'això, torna a la pàgina de configuració de SAML2.
  6. Feu clic al botó Bloqueja el *certificat*.
  7. Al terminal executeu l'script per autoconfigure: acoblador exec isard-sso-admin python3 moodle_saml.py
  8. L'última cosa és purgar la memòria cau de moodle: ]]femida l'script php-fpm7 de l'acoblador Exec, feu-ho a través de moodle ui]]

- Nextcloud: Automatitzada. Després d'acabar el *make all* hauria d'estar llest. En cas que falli refereix a isard-sso/docs.
- Wordpress: Pràcticament automatitzat. Després d'acabar el *make all* hauria d'estar llest i només caldrà activar el connector. En cas que falli refereix a isard-sso/docs.

## Instal.lació estesa

Podeu iniciar aquest projecte en qualsevol servidor amb docker &  docker-compose (qualsevol sistema operatiu hauria de funcionar). Per a instal·lar aquests paquets a la vostra distribució, consulteu el funcionament de docker & docker-compose a la documentació oficial i a la carpeta sysadm teniu  alguns scripts d'automatització

Qualsevol distribució hauria de funcionar però,  si voleu utilitzar els nostres scripts sysadm per instal·lar docker & docker-compose , utilitzeu Debian Buster (10).

### Clonar els submòduls

```
git clone https://gitlab.com/digitaldemocratic/digitaldemocratic/
cd digitaldemocratic
git submodule update --init --recursive
```

### docker

Referiu-vos a la documentació oficial (https://docs.docker.com/engine/install/) o utilitzeu l'script a la carpeta sysadm per a Debian Buster (10).

### docker-compose

Referiu-vos a la documentació oficial (https://docs.docker.com/compose/install/) o utilitzeu l'script a la carpeta sysadm per a Debian Buster (10).

### Configuració

Copieu digitaldemocratic.conf.exemple a digitaldemocratic.conf i editeu-lo per satisfer les vostres necessitats. Com a mínim (per a desenvolupament) heu d'adaptar la variable DOMAIN al vostre domini arrel.

- PRODUCCIÓ: Necessiteu un dns multidomini (o redirigeix els subdominis múltiples) a la vostra màquina d'amfitrió.
- Desenvolupament: Editeu el fitxer /etc/hosts i afegiu els subdominis per a propòsits de proves locals.

#### Subdominis

- Keycloak: sso.<yourdomain.org>
- Api: api.<yourdomain.org>
- Moodle: moodle.<yourdomain.org>
- Nextcloud: nextcloud.<yourdomain.org>
- Wordpress: wp.<yourdomain.org>
- Onlyoffice: oof.<yourdomain.org>
- Etherpad: pad.<yourdomain.org>
- (opcional) FreeIPA: ipa.<yourdomain.org>

### Personalització

Copia recursivament la carpeta *custom.sample* a *custom* i edita els fitxers yaml de personalització i menú i substitueix les imatges.

### Iniciar el projecte

La primera vegada (i si voleu actualitzar a la última versió posteriorment) executeu:

```
./dd-ctl repo-update
```

I després:

```
./dd-ctl all
```

Posteriorment podreu iniciar o aturar amb:

```
./dd-ctl down
./dd-ctl up
```

### Integració

Llegiu el fitxer [SAML_README.md](https://gitlab.com/isard/isard-sso/-/blob/master/docs/SAML_README.md) a la carpeta isard-sso/docs per integrar totes les aplicacions. Ara el  Nextcloud i el Wordpress s'haurien d'integrar automàticament amb el Keycloak.
