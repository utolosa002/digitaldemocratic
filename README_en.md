[English](https://gitlab.com/digitaldemocratic/digitaldemocratic/-/blob/master/README_en.md) - [Català](https://gitlab.com/digitaldemocratic/digitaldemocratic/-/blob/master/README.md) 

### License

AGPLv3 (https://www.gnu.org/licenses/agpl-3.0.en.html)

### Credits

Pilot project of the Democratic Digitalization Plan led by Xnet and promoter families. Software created by IsardVDI and 3iPunt with the collaboration of MaadiX.net, Affac, the Barcelona City Hall and the Barcelona Education Consortium.

# What's this

This project allows to bring up with ease a full identity provider and many apps to have an environment thought for schools and universities. The project will provide an integrated solution to handle the common environment in education:

- **Classrooms**: A Moodle instance with custom theme and custom plugins.
- **Cloud drive**: A Nextcloud instance with custom theme and custom plugins.
- **Documents**: An Onlyoffice instance integrated with Nextcloud.
- **Web pages**: A Wordpress instance with custom theme and custom plugins.
- **Pad**: An Etherpad instance integrated with Nextcloud.
- **Conferences**: A BigBlueButton integrated with Moodle and Nextcloud. This needs a standalone host.
- **Forms**: A forms Nextcloud plugin.
- ... (some apps like jitsi or BigBlueButton are not fully integrated right now)

|                              |                                 |
| ---------------------------- | ------------------------------- |
| ![](docs/img/classrooms.png) | ![](docs/img/cloud_storage.png) |

# Administration interface

Now there is an administration interface that allows to easily manage users and groups and keep it in sync between applications. This is done by executing actions on the different applications apis.

| ![](docs/img/admin_sync.png) | ![](docs/img/admin_user_edit.png) |
| ---------------------------- | --------------------------------- |

To easily migrate and insert users and groups to the system there are also two provided imports:

- From Google Admin Console as a JSON dump
- From a CSV file

This admin interface is now in an alpha status but allows to manage users in sync between Keycloak, Moodle and Nextcloud.

# Project status

Functional, but we are still working on and will have lots of improvements in the upcoming months. Some automatizations need to be done, specially with SAML integration in moodle and Keycloak.

Your collaboration is welcome! Just *fork* this repo to develop and do a PR or open an *issue*.

# DigitalDemocratic Documentation

This documentation is written in Markdown using [MkDocs+Gitlab](https://gitlab.com/pages/mkdocs).

See `docs` directory for Markdown files or the [auto-built site](https://digitaldemocratic.gitlab.io/digitaldemocratic).

## Quick start

```
cp digitaldemocratic.conf.sample digitaldemocratic.conf
```

Changes default passwords

```
./securize_conf.sh
```

Edit digitaldemocratic.conf file variables to suit your needs.

```
cp -R custom.sample custom
```

Edit and replace files to personalize the system.

The first time execute:

```
./dd-ctl repo-update
```

And then:

```
./dd-ctl all
```

NOTE: The SAML Auth in plugin is now automated:
- Moodle: Not fully automated.
    1. Login to Moodle as admin via: https://moodle.\<domain\>/login/index.php?saml=off
    2. Go to authentication configuration: https://moodle.\<domain\>/admin/settings.php?section=manageauths
    3. Enable SAML2 clicking the eye.
    4. Click on *configuration* on SAML2
    5. Click on the *Regenerate certificate* button inside the form. After that go back to SAML2 configuration page.
    6. Click on the *Lock certificate* button.
    7. In the terminal execute the script to autoconfigure: docker exec isard-sso-admin python3 moodle_saml.py
    8. The last thing is to purge moodle cache: [[missing docker exec php-fpm7 script, do it through moodle web ui]]

- Nextcloud: Fully automated. After finishing the *make all* it should be ready. In case it fails refer to isard-sso/docs.
- Wordpress: Practically automated. After finishing the *make all* it should be ready and you will only need to activate the plugin. In case it fails refer to isard-sso/docs.

## Extended Installation

You can start this project in any docker & docker-compose host (any OS should work). To install those packages in your distro refer to docker & docker-compose in this documentation and in the sysadm folder you have some scripts.

Any distro should work but, if you want to use our sysadm scripts to install docker & docker compose use Debian Buster (10).

### Clone and submodules

```
git clone https://gitlab.com/digitaldemocratic/digitaldemocratic/
cd digitaldemocratic
git submodule update --init --recursive
```

### docker

Refer to the official documentation (https://docs.docker.com/engine/install/) or use our sysadm script if you are on a Debian Buster (10).

### docker-compose

Refer to the official documentation (https://docs.docker.com/compose/install/) or use our sysadm script if you are on a Debian Buster (10).

### Setup

Copy digitaldemocratic.conf.sample to digitaldemocratic.conf and edit to satisfy your needs. At least (for development) you should adapt the DOMAIN envvar to your root domain.

- PRODUCTION: You'll need a multidomain dns (or redirect all the subdomains) to your server IP.
- DEVELOPMENT: You'll have to edit your /etc/hosts and add all the required domains to your development server IP.

#### Subdomains

- Keycloak: sso.<yourdomain.org>
- Api: api.<yourdomain.org>
- Moodle: moodle.<yourdomain.org>
- Nextcloud: nextcloud.<yourdomain.org>
- Wordpress: wp.<yourdomain.org>
- Onlyoffice: oof.<yourdomain.org>
- Etherpad: pad.<yourdomain.org>
- (optional) FreeIPA: ipa.<yourdomain.org>

NOTE: To test it in your computer without a domain you'll need to simulate a domain and set it in digitaldemocratic.conf and then add to your /etc/hosts this subdomains to your IP.

For example:

```
echo '192.168.0.5 mydomain.org admin.mydomain.org nextcloud.mydomain.org moodle.mydomain.org sso.mydomain.org api.mydomain.org wp.mydomain.org pad.mydomain.org oof.mydomain.org' >> /etc/hosts
```

### Customization

Recursively copy the *custom.sample* folder to *custom* and edit the yaml personalization files and menu and substitute the image.

### Start the project

The first time (and if you want to upgrade to the latest version later) you should execute:

```
./dd-ctl update-repo
```

And after:

```
./dd-ctl all
```

Then you can start or stop with:

```
./dd-ctl down
./dd-ctl up
```

### Integration

Read the [SAML_README.md](https://gitlab.com/isard/isard-sso/-/blob/master/docs/SAML_README.md) file in isard-sso/docs folder to integrate all the applicactions. Now, Nextcloud and Wordpress should integrate automatically with the Keycloak.
